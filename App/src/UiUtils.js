/*!
  THANKS TO:
  https://forum.qt.io/topic/106362/best-way-to-set-text-color-for-maximum-contrast-on-background-color
*/

function lightDark(background, lightColor, darkColor) {
    return isDarkColor(background) ? lightColor : darkColor;
}

function isDarkColor(background) {
    var temp = Qt.darker(background, 1);
    var a = 1 - (0.299 * temp.r + 0.587 * temp.g + 0.114 * temp.b);
    return temp.a > 0 && a >= 0.3;
}

function refreshModel(model, data) {
    model.clear();
    data.forEach(function(d) {
        model.append(d);
    });
}
