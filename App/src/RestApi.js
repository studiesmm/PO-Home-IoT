function RestApi(url) {
    this.url = url;

    this.httpRequest = function(url, body, method, headers, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onreadystatechange = (function(myxhr) {
            return function() {
                if(myxhr.readyState === myxhr.DONE) callback(myxhr);
            }
        })(xhr);

        headers.forEach(function(header) {
            xhr.setRequestHeader(header.key, header.value);
        });

        xhr.send(body);
    } //httpRequest
}
