function Authenticator(restApi) {
    this._restApi = restApi;
    this.user = '';
    this.isLoggedIn = false;
    this.jwt = '';

    this.logIn = function(accname, passwd, cb, cbb) {
        var body = "name=" + accname + "&password=" + passwd;
        var header = {
            key: "Content-Type",
            value: "application/x-www-form-urlencoded",
        }
        this._restApi.httpRequest((this._restApi.url + "/login"), body, "POST", [header], function(res) {
            if(res.status === 200) {
                var resp = JSON.parse(res.responseText);
                cb(resp, accname, cbb);
            }
        });
    }
    var _this = this;
    this.onReceive = function(res, accname, cbb) {
        _this.jwt = res.token;
        _this.user = accname;
        _this.isLoggedIn = true;
        cbb();
    }
}
