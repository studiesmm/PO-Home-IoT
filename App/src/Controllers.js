function DeviceController(restApi) {
    this._restApi = restApi;
    this.availableDevices = [];

    this.fetchDevices = function(cb) {

        var header = {
            key: "Content-Type",
            value: "application/x-www-form-urlencoded",
        }

        this._restApi.httpRequest((this._restApi.url + "/devices"), "", "GET", [header], function(res) {
            var resp = JSON.parse(res.responseText);
            cb(resp);
        });
    }

    var _this = this;

    this.onReceive = function(res) {
        _this.availableDevices = res;
    }

    this.getDevicesByType = function(dev_type) {
        var devices = [];
        _this.availableDevices.forEach(function(device) {
            if(device.device_type === dev_type) {
                devices.push(device);
                }
        });
        return devices;
    }
}

function LightsController(restApi) {
    this._restApi = restApi;

    this.setLight = function(id, color, intensity, jwt, cb) {
        var headers =
                [
                    {
                      key: "Content-Type",
                      value: "application/x-www-form-urlencoded",
                    },
                    {
                      key: "x-access-token",
                      value: jwt,
                    }
                ]
        var body = "id=" + id + "&red=" + color.r + "&green=" + color.g
                    + "&blue=" + color.b + "&intensity=" + intensity;

        this._restApi.httpRequest((this._restApi.url + '/devices/lights/set'), body, "POST", headers, function(res) {
            cb(res);
        });
    }
}
