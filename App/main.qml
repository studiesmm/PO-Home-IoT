import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Material 2.0

import 'models'
import 'delegates'
import 'src/Controllers.js' as Controllers
import 'src/RestApi.js' as RestApi
import 'src/Authenticator.js' as Authenticator
import 'src/UiUtils.js' as UiUtils

ApplicationWindow {
    id: mainw
    visible: true
    width: 640
    height: 480
    title: qsTr("Tabs")

    Material.theme: Material.light
    Material.accent: "#263238"

    property var url: "http://195.22.96.51/api"
    property var restApi: new RestApi.RestApi(mainw.url);
    property var deviceController: new Controllers.DeviceController(mainw.restApi);
    property var authenticator: new Authenticator.Authenticator(mainw.restApi);
    property var lightsController: new Controllers.LightsController(mainw.restApi);


    Timer {
        id: refresher
        interval: 100; running: true; repeat: false; triggeredOnStart: true;
        onTriggered: {
            deviceController.fetchDevices(deviceController.onReceive);
            refresh_delay.start();
        }
    }

    Timer {
        id: first
        interval: 500; running: true; repeat: false; triggeredOnStart: true;
        onTriggered: {
            deviceController.fetchDevices(deviceController.onReceive);
            refresh_delay.start();
        }
    }
    Timer {
        id: refresh_delay
        interval: 200; running: false; repeat: false;
        onTriggered: {
            var sensors = deviceController.getDevicesByType('sensors');
            var lights = deviceController.getDevicesByType('lights');
            UiUtils.refreshModel(sensors_model, sensors);
            UiUtils.refreshModel(lights_model, lights);
            console.log('refreshed');
        }
    }

    LoginScreen {
        visible: true
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        DeviceModel {
            id: lights_model
        }
        DeviceModel {
            id: sensors_model
        }

        Page1Form {
            ListView {
                anchors.fill: parent
                model: lights_model
                delegate:
                    LightDelegate {
                        id: lights_delegate
                        anchors.left: parent.left
                        anchors.right: parent.right
                }
                onFlickEnded: {
                    refresher.start();
                }
            }
        }

        Page2Form {
            ListView {
                anchors.fill: parent
                model: sensors_model
                delegate:
                    SensorDelegate {
                        id: sensor_delegate
                        anchors.left: parent.left
                        anchors.right: parent.right
                }
                onFlickEnded: {
                   refresher.start();
                }
            }
        }
    }
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Lights")

        }

        TabButton {
            text: qsTr("Sensors")
        }

    }
}
