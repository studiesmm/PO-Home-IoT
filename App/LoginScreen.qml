import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.0
import QtQuick.Window 2.2 
import QtQuick.Controls.Material 2.0
Window {
    id: loginScr
    // disabling bug check - onClosing not recognized (Qt bug)
    // @disable-check M16
    onClosing: {
        if(mainw.authenticator.isLoggedIn === false) {
            close.accepted = false;
        }
    }
    Material.accent: mainw.Material.accent

    width: mainw.width
    height: mainw.height


    Text {
        id: passwordText
        x: 0
        y: 208
        text: qsTr("Password")
        anchors.top: usernameInput.bottom
        anchors.topMargin: 60
        horizontalAlignment: Text.AlignHCenter
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        font.pixelSize: 24
    }

    Text {
        id: usernameText
        x: 0
        y: 60
        text: qsTr("Username")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 60
        font.pixelSize: 24
    }

    TextField {
        id: usernameInput
        x: 30
        y: 108
        text: qsTr("")
        horizontalAlignment: Text.AlignHCenter
        placeholderText: "username"
        anchors.top: usernameText.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.topMargin: 20
        anchors.rightMargin: 30

    }

    TextField {
        id: passwordInput
        x: 30
        y: 256
        text: qsTr("")
        horizontalAlignment: Text.AlignHCenter
        placeholderText: "password"
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.top: passwordText.bottom
        anchors.topMargin: 20
        echoMode: TextInput.Password
    }

    Button {
        id: loginButton
        x: 270
        y: 336
        text: qsTr("Log In")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: passwordInput.bottom
        anchors.topMargin: 40

        onClicked: {
            authenticator.logIn(usernameInput.text,
                                passwordInput.text,
                                authenticator.onReceive, function() {
                                    loginScr.close();
                                });
        }
    }
}

