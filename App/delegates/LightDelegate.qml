import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.0
import '../src/UiUtils.js' as UiUtils
import '../src/Controllers.js' as Control
DeviceDelegate {
    id: deviceDelegate
    height: 150
    bcolor: Qt.rgba(red, green, blue, 1)
    property int lightid: id
    Text {
        text: id
        font.pointSize: 20
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        anchors.leftMargin: 20
        color: UiUtils.lightDark(bcolor, "white", "black")
    }

    Switch {
        id: light_switch
        x: 513
        y: 26
        checked: {
            intensity === 1 ? true : false
        }
        anchors.right: parent.right
        anchors.rightMargin: 20
        onClicked: {
           checked ?
           mainw.lightsController.setLight(lightid, bcolor, 1, mainw.authenticator.jwt, function(res) {
                refresher.start()
           })
           :
           mainw.lightsController.setLight(lightid, bcolor, 0, mainw.authenticator.jwt, function(res) {
                refresher.start()
           })
        }
    }

    ColorDialog {
        id: color_dialog
        visible: false
        title: "Choose light color"
        onAccepted: {
            color_dialog.visible = false
            var pickedColor = color_dialog.color
            light_switch.checked ?
            mainw.lightsController.setLight(lightid, pickedColor, 1, mainw.authenticator.jwt, function(res) {
                refresher.start()
            })
            :
            mainw.lightsController.setLight(lightid, pickedColor, 0, mainw.authenticator.jwt, function(res) {
                refresher.start()
            })
        }
    }

    RoundButton {
        id: roundButton
        x: 563
        y: 79
        text: ""
        anchors.horizontalCenter: light_switch.horizontalCenter

        Image {
            id: image
            scale: 1
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            anchors.bottomMargin: 5
            anchors.topMargin: 5
            anchors.fill: parent
            source: "../rsc/palette24p.png"
            fillMode: Image.Stretch
        }
        onClicked: {
            color_dialog.visible = true;
        }

    }
}

/*##^##
Designer {
    D{i:4;anchors_height:100;anchors_width:100}
}
##^##*/
