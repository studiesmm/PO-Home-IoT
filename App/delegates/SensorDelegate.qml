import QtQuick 2.0
import QtQuick.Controls.Material 2.0
import QtGraphicalEffects 1.0
import '../src/UiUtils.js' as UiUtils

DeviceDelegate {
    id: deviceDelegate
    property string sensortime: time
    property string sensorval: value
    bcolor: "#4c8c4a"

    Text {
        id: senstext1
        y: 68
        text: sensorval
        anchors.horizontalCenterOffset: 100
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenterOffset: 0
        anchors.verticalCenter: parent.verticalCenter
        color: UiUtils.lightDark(bcolor, "white", "black")
        font.pixelSize: 25
    }

    Text {
        id: senstext2
        x: 32
        y: 67
        text: sensortime
        anchors.horizontalCenterOffset: -100
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        color: UiUtils.lightDark(bcolor, "white", "black")
        font.pixelSize: 25
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2;anchors_x:32}
}
##^##*/
