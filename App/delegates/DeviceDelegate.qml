import QtQuick 2.0
import QtGraphicalEffects 1.0
Item {
        height: 150
        width: parent.width
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        id: devdelegate
        property color bcolor: "grey"

        DropShadow {
            anchors.fill: rectangle
            horizontalOffset: 2
            verticalOffset: 2
            radius: 8
            samples: 17
            color: "#80000000"
            source: rectangle
        }

        Rectangle {
            id: rectangle
            color: devdelegate.bcolor
            anchors.fill: parent
            anchors.bottomMargin: 10
            anchors.topMargin: 10
            radius: 6
        }
}
